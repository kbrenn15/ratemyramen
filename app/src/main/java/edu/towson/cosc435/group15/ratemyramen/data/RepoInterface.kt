package edu.towson.cosc435.group15.ratemyramen.data

import edu.towson.cosc435.group15.ratemyramen.model.Ramen

interface RepoInterface {
    suspend fun getRamen(): List<Ramen>
    suspend fun deleteRamen(ramen:Ramen)
    suspend fun addRamen(ramen: Ramen)
    suspend fun setRating(ramen: Ramen, rating: Int)
}