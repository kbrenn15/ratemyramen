package edu.towson.cosc435.group15.ratemyramen.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Red1 = Color(0xFFDD442C)
val Purple1 = Color(0xFFBBB4D6)
val Yellow1 = Color(0xFFF3DE8A)
val Black1 = Color(0xFF1F1F1F)
val White1 = Color(0xfff9f8f8)
val Red2 = Color(0xFFFF4F4B)