package edu.towson.cosc435.group15.ratemyramen.model


import com.google.gson.annotations.SerializedName

data class RamenApiObject(
    @SerializedName("title")
    val title: String,

    @SerializedName("brand")
    val brand: String,

    @SerializedName("images")
    val imageUrl: String
    )