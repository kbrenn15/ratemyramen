package edu.towson.cosc435.group15.ratemyramen.data;


import androidx.room.*;



import edu.towson.cosc435.group15.ratemyramen.model.Ramen;

@Dao
interface RamenDao {
    @Query("Select * FROM ramen")
    suspend fun getRamen(): List<Ramen> //livedata for any changes made to db

    @Insert
    suspend fun addRamen (ramen: Ramen)

    @Delete
    suspend fun deleteRamen(ramen: Ramen)

    @Update
    suspend fun updateSelected(ramen:Ramen)

}


@Database (entities = [Ramen::class], version = 2, exportSchema = false)
abstract class RamenDB : RoomDatabase(){
    abstract fun ramenDao(): RamenDao
}



