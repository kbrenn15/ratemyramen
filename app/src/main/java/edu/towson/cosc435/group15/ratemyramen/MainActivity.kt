package edu.towson.cosc435.group15.ratemyramen

import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.core.content.FileProvider
import androidx.lifecycle.lifecycleScope
import edu.towson.cosc435.group15.ratemyramen.ui.navigation.RamenNav
import edu.towson.cosc435.group15.ratemyramen.ui.theme.RateMyRamenTheme
import java.io.File


class MainActivity() : ComponentActivity() {

    var imageUriState = mutableStateOf<Uri?>(null)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RateMyRamenTheme {

                // A surface container using the "background" color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                 RamenNav({ takeImage() }, {selectImageFromGallery()}, {getUri()}, {setUri(null)})
                }
            }

        }
    }



    //Source https://medium.com/codex/how-to-use-the-android-activity-result-api-for-selecting-and-taking-images-5dbcc3e6324b
    private val takeImageResult = registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
        if (isSuccess) {
            latestTmpUri?.let { uri ->
                imageUriState.value = uri
            }
        }
    }

    private val selectImageFromGalleryResult = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
        uri?.let { imageUriState.value = uri }
    }

    private var latestTmpUri: Uri? = null

    fun takeImage() {
        lifecycleScope.launchWhenStarted {
            getTmpFileUri().let { uri ->
                latestTmpUri = uri
                takeImageResult.launch(uri)
            }
        }
    }

    fun selectImageFromGallery() = selectImageFromGalleryResult.launch("image/*")

    fun getUri():Uri?{
        return imageUriState.value
    }

    fun setUri(uri: Uri?){
        imageUriState.value = uri
    }

    private fun getTmpFileUri(): Uri {
        val tmpFile = File.createTempFile("tmp_image_file", ".png", cacheDir).apply {
            createNewFile()
            deleteOnExit()
        }

        return FileProvider.getUriForFile(applicationContext, "${BuildConfig.APPLICATION_ID}.provider", tmpFile)
    }


}




