package edu.towson.cosc435.group15.ratemyramen.ui.importqr

import android.Manifest
import android.util.Log
import android.view.ViewGroup
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.google.common.util.concurrent.ListenableFuture
import edu.towson.cosc435.group15.ratemyramen.model.RamenApiObject
import edu.towson.cosc435.group15.ratemyramen.ui.importmanual.ManualImportViewModel
import edu.towson.cosc435.group15.ratemyramen.ui.navigation.Routes
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun QRImportScreen (
    navController: NavHostController,
    manualVm: ManualImportViewModel = viewModel(),
    qrVm: QrImportViewModel = viewModel()){
    val cameraPermissionState = rememberPermissionState(permission = Manifest.permission.CAMERA)

    val context = LocalContext.current
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        //Home Button Row
        Column(
            modifier = Modifier
                .padding(bottom = 80.dp, top = 10.dp, start = 10.dp)
                .fillMaxWidth(),

            ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Start,
            ) {
                IconButton(onClick = {
                    navController.navigate(Routes.HomeScreen.route)
                }) {
                    Icon(
                        Icons.Filled.Home, contentDescription = "Localized description",
                        Modifier.size(50.dp),
                        tint = Color.Gray
                    )
                }
            }
        }
        //Request camera permission button
        Spacer(Modifier.height(10.dp))
        if (cameraPermissionState.hasPermission) {
            CameraPreview(qrVm, manualVm, navController)
        } else {
            Button(
                onClick = {
                    cameraPermissionState.launchPermissionRequest()
                    Log.d("TAG", "Requesting Permission ")
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFFF3DE8A))
            ) {
                Text(text = "Allow Camera")
            }
        }

    }
}
//Source: https://medium.com/@dpisoni/building-a-simple-photo-app-with-jetpack-compose-camerax-and-coroutines-part-2-camera-preview-cf1d795129f6
@Composable
fun CameraPreview(qrVm: QrImportViewModel = viewModel(), manualVm: ManualImportViewModel, navController: NavHostController){
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    var preview by remember { mutableStateOf<Preview?>(null)}
    val barCodeVal = remember { mutableStateOf("") }
    val barcodeFound = remember { mutableStateOf<Boolean>(false)}
    var ramenApiObject by remember { mutableStateOf(RamenApiObject("null", "null", "null"))}
    Column(
        Modifier.fillMaxWidth()
    ) {
        //import button and api return image preview
        Row(
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly,
        ) {
            if (ramenApiObject.title != "null"){
                Button(onClick = {
                    manualVm.qrInit(ramenApiObject)
                    navController.navigate(Routes.ManualImportScreen.route)
                }) {
                    Text(text = "Import")
                }
                Image(
                    painter = rememberAsyncImagePainter(ramenApiObject.imageUrl),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )
            }
        }
        if (barcodeFound.value){
            Column(Modifier.fillMaxWidth()) {
                Text(text = "Barcode found: ${barCodeVal.value}")
            }

        }
        //Barcode scanning
        AndroidView(
            factory = { AndroidViewContext ->
                PreviewView(AndroidViewContext).apply{
                    this.scaleType = PreviewView.ScaleType.FILL_CENTER
                    layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                    )
                    implementationMode = PreviewView.ImplementationMode.COMPATIBLE
                }
            },
            modifier = Modifier
                .fillMaxSize(),
            update = { previewView ->
                val cameraSelector: CameraSelector = CameraSelector.Builder()
                    .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                    .build()
                val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
                val cameraProviderFuture: ListenableFuture<ProcessCameraProvider> =
                    ProcessCameraProvider.getInstance(context)

                cameraProviderFuture.addListener({
                    preview = Preview.Builder().build().also {
                        it.setSurfaceProvider(previewView.surfaceProvider)
                    }
                    val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
                    val barcodeAnalyser = QRScanner {barcodes ->
                        barcodes.forEach { barcode ->
                            barcode.rawValue?.let {barcodeValue ->
                                barCodeVal.value = barcodeValue
                                qrVm.barcodeLookup(barCodeVal.value , context)
                                ramenApiObject = qrVm.tempRamenApiObject
                                barcodeFound.value = true
                            }
                        }
                    }
                    val imageAnalysis: ImageAnalysis = ImageAnalysis.Builder()
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                        .build()
                        .also {
                            it.setAnalyzer(cameraExecutor, barcodeAnalyser)
                        }

                    try {
                        cameraProvider.unbindAll()
                        cameraProvider.bindToLifecycle(
                            lifecycleOwner,
                            cameraSelector,
                            preview,
                            imageAnalysis
                        )
                    } catch (e: Exception) {
                        Log.d("TAG", "CameraPreview: ${e.localizedMessage}")
                    }
                }, ContextCompat.getMainExecutor(context))

            }
        )
    }

}
