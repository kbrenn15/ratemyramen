package edu.towson.cosc435.group15.ratemyramen.ui

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue

@Composable
fun DeleteBox(
    title: String,
    message: String,
    deleteVM: DeleteVM
){
    val display by deleteVM.displayMessage
    if(display){
        AlertDialog(
            onDismissRequest ={ deleteVM.dismissMessage()},
            title = { Text(title) },
            text = { Text(message) },
            confirmButton = {
                Button({
                    deleteVM.onDeleteConfirm()
                }
                )
                {
                    Text("Delete Ramen")
                }
            },
            dismissButton = {
                Button({deleteVM.dismissMessage()})
                {Text("Cancel")}
            }
        )
    }
}