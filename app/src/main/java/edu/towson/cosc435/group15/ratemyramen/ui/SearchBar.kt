package edu.towson.cosc435.group15.ratemyramen.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchBar(onFilter: (String) -> Unit) {
    Row(
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(18.dp),

    ){
        var filter: String by remember {mutableStateOf("")}
        val keyboardController = LocalSoftwareKeyboardController.current
        OutlinedTextField(value = filter, onValueChange = {f: String -> filter = f}, singleLine = true,
        modifier = Modifier
            .width(275.dp)
            .padding(end = 18.dp),
            keyboardActions = KeyboardActions(
                onDone = {
                    keyboardController?.hide()
                }
            ),
            label = { Text("Filter Reviews") },
        )
        Button(
            modifier = Modifier
                .padding(4.dp),
            onClick = { onFilter(filter) },
            colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary)
            ) {
                Text(text = "Filter", color = Color.Gray)
        }
    }
}