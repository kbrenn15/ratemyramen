package edu.towson.cosc435.group15.ratemyramen.ui.importqr

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope

import edu.towson.cosc435.group15.ratemyramen.model.RamenApiObject
import kotlinx.coroutines.launch

class QrImportViewModel(app: Application) : AndroidViewModel(app)  {
    val apiLookup = ApiLookup()

    var tempRamenApiObject = RamenApiObject("null", "null", "null")
    //Takes input of barcode and context and sets RamenApiObject based on api return
    fun barcodeLookup (barcodeId: String, context: Context){
        viewModelScope.launch {
           tempRamenApiObject = apiLookup.barcodeLookup(barcodeId, context)
        }
    }
}