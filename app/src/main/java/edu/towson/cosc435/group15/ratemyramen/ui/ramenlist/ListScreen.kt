package edu.towson.cosc435.group15.ratemyramen.ui.ramenlist

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import edu.towson.cosc435.group15.ratemyramen.RamenRow
import edu.towson.cosc435.group15.ratemyramen.model.Ramen
import edu.towson.cosc435.group15.ratemyramen.ui.DeleteBox
import edu.towson.cosc435.group15.ratemyramen.ui.DeleteVM
import edu.towson.cosc435.group15.ratemyramen.ui.SearchBar
import edu.towson.cosc435.group15.ratemyramen.ui.navigation.Routes

@Composable
fun ListScreen(
                navController: NavHostController,
                ramen: List<Ramen>,
                deleteVM: DeleteVM,
                onDelete: suspend(Ramen) -> Unit,
                onSelect: (Ramen) -> Unit,
                onFilter:(String) -> Unit
) {
    val selection = remember { mutableStateOf(Filter.All) }
    val selectedRating = remember { mutableStateOf(0)}
    var displayRamen = ramen
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {
        DeleteBox("Delete?", "Are you sure you want to Delete entry?", deleteVM)

        //Home Button Row
        Column(
            modifier = Modifier
                .padding(bottom = 10.dp, top = 10.dp, start = 10.dp)
                .fillMaxWidth(),

            ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                IconButton(onClick = {
                    navController.navigate(Routes.HomeScreen.route)
                }) {
                    Icon(
                        Icons.Filled.Home, contentDescription = "Localized description",
                        Modifier.size(50.dp),
                        tint = Color.Gray
                    )
                }
                Column() {
                    IconButton(
                        onClick = {
                            navController.navigate(Routes.ImportMenuScreen.route)

                        }) {
                        Icon(
                            Icons.Filled.Add, contentDescription = "Localized description",
                            Modifier.size(50.dp).padding(4.dp),
                            tint = Color.Gray
                        )
                    }
                }
            }
        }
        //Title Row
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
        ) {
            Text(
                text = "Your Ramen List",
                color = MaterialTheme.colors.primary,
                fontSize = 30.sp,
                modifier = Modifier.padding(8.dp)
            )
        }
        //Search Bar
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
        ) {
            SearchBar(onFilter = onFilter)

        }
        //Radial Buttons
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            RadioButton(
                modifier = Modifier.padding(3.dp),
                selected = selection.value == Filter.All,
                onClick = {
                    selection.value = Filter.All
                    selectedRating.value = 0
                }
            )
            Text(
                text = "All",
                modifier = Modifier.padding(end = 15.dp)
            )
            RadioButton(
                modifier = Modifier.padding(3.dp),
                selected = selection.value == Filter.Five,
                onClick = {
                    selection.value = Filter.Five
                    selectedRating.value = 5
                }
            )
            Text(
                text = "5",
                modifier = Modifier.padding(end = 15.dp),
                color = MaterialTheme.colors.primary

            )

            RadioButton(
                modifier = Modifier.padding(3.dp),
                selected = selection.value == Filter.Four,
                onClick = {
                    selection.value = Filter.Four
                    selectedRating.value = 4
                }
            )
            Text(
                text = "4",
                modifier = Modifier.padding(end = 15.dp),
                color = MaterialTheme.colors.primary
            )

            RadioButton(
                modifier = Modifier.padding(3.dp),
                selected = selection.value == Filter.Three,
                onClick = {
                    selection.value = Filter.Three
                    selectedRating.value = 3
                }
            )
            Text(
                text = "3",
                modifier = Modifier.padding(end = 15.dp),
                color = MaterialTheme.colors.primary
            )

            RadioButton(
                modifier = Modifier.padding(3.dp),
                selected = selection.value == Filter.Two,
                onClick = {
                    selection.value = Filter.Two
                    selectedRating.value = 2
                }
            )
            Text(
                text = "2",
                modifier = Modifier.padding(end = 15.dp),
                color = MaterialTheme.colors.primary
            )

            RadioButton(
                modifier = Modifier.padding(3.dp),
                selected = selection.value == Filter.One,
                onClick = {
                    selection.value = Filter.One
                    selectedRating.value = 1
                }
            )
            Text(
                text = "1",
                modifier = Modifier.padding(end = 15.dp),
                color = MaterialTheme.colors.primary
            )
        }
        //List
        LazyColumn(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(4.dp),
            horizontalAlignment = Alignment.Start,
            contentPadding = PaddingValues(horizontal = 12.dp, vertical = 50.dp),
        ) {

            if (selectedRating.value != 0) {
                displayRamen =
                    ramen.filterIndexed { index, ramen -> (ramen.rating == selectedRating.value) }
            }
            itemsIndexed(displayRamen) { index, ramen ->
                RamenRow(index, ramen, { index ->
                    deleteVM.displayDeleteConfirm(onConfirm = { onDelete(ramen) })
                }, onSelect = { onSelect(ramen) })

            }

        }
    }
}

enum class Filter {
    All, Five, Four, Three, Two, One
}

