
package edu.towson.cosc435.group15.ratemyramen.ui.importmanual

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.towson.cosc435.group15.ratemyramen.ui.navigation.Routes

@Composable
fun ManualImageImportScreen (
    navController: NavHostController,
    takeImage:() -> Unit,
    selectImageFromGallery:() -> Unit,
) {
    Box(contentAlignment =  Alignment.Center, modifier = Modifier.fillMaxSize()){
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Button(
                onClick = {
                    takeImage()
                    navController.navigate(Routes.ManualImportScreen.route)
                          },
                modifier = Modifier.padding(vertical = 8.dp)
            ) {
                Text(text = "Open Camera")
            }
            Button(
                onClick = {
                    selectImageFromGallery()
                    navController.navigate(Routes.ManualImportScreen.route)},
                modifier = Modifier.padding(vertical = 8.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary)

            ) {
                Text(text = "Select Image")
            }

        }
    }

}