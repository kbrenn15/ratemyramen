package edu.towson.cosc435.group15.ratemyramen.ui.importqr

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.widget.Toast
import com.google.gson.JsonElement
import com.google.gson.JsonParser.parseString
import edu.towson.cosc435.group15.ratemyramen.model.RamenApiObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request


class ApiLookup {
    //Handles calling of both the api call to get the JSON from the barcodelookup API, then passes the JSON to the the JSON parser that outputs a RamenApi Object
    suspend fun barcodeLookup (barcodeId: String, context: Context):RamenApiObject{
        val connected: Boolean = checkForInternet(context)
        //Checks if connected to internet
        if(connected.equals(true)){
            Log.d("Tag", "getting object with id $barcodeId")
            //Calls barcodelookup.com Api
            val json = getRamenJson(barcodeId)

            //test static json, to avoid using api calls during testing
            //val json = """{"products":[{"title":"Kokomen Spicy Chicken Ramen,4.23 Oz (Pack of 16)","brand":"Paldo","images":["https://images.barcodelookup.com/36338/363380423-1.jpg"]}]}"""

            Log.d("Tag", "returning ramen Object")
            //Returns RamenApi Object create from passed json
            return getRamenApiObject(json)
        }else{
            Log.d("Tag", "failed to connect to network")
            val toast = Toast.makeText(context, "Please connect to the internet import", Toast.LENGTH_LONG )
            toast.show()
            return RamenApiObject("null", "null", "null" )
        }
    }

    //Calls barcodelookup.com Api
    suspend fun getRamenJson (barcodeId: String): String?{
        val getURL = "https://api.barcodelookup.com/v3/products?barcode=${barcodeId}&formatted=y&key=568le4s8697dqubtgm0s4hc4p6l83p"
        return  withContext(Dispatchers.IO){
            val myClient = OkHttpClient()
            val myRequest = Request.Builder()
                .url(getURL)
                .get()
                .build()
            val response = myClient.newCall(myRequest).execute()
            val ramenJson = response.body?.string()
            Log.d("TAG", " json return: $ramenJson")
            ramenJson
        }
    }

    //Returns RamenApi Object create from passed json
    fun getRamenApiObject(json: String?): RamenApiObject {
        var title = "null"
        var brand = "null"
        var imageUrl = "null"
        val jelement: JsonElement = parseString(json)
        var jobject = jelement.asJsonObject
        var jarray = jobject.getAsJsonArray("products")
        jobject = jarray[0].asJsonObject
        title = jobject["title"].asString
        Log.d("TAG", "title: $title")
        brand = jobject["brand"].asString
        Log.d("TAG", "brand: $brand")
        jarray = jobject.getAsJsonArray("images")
        imageUrl = jarray[0].asString
        Log.d("TAG", "images: $imageUrl")
        return RamenApiObject(title, brand, imageUrl)
    }

    //Checks if connected to internet
    //Source: https://www.geeksforgeeks.org/how-to-check-internet-connection-in-kotlin/
    fun checkForInternet(context: Context): Boolean {

        // register activity with the connectivity manager service
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        // if the android version is equal to M
        // or greater we need to use the
        // NetworkCapabilities to check what type of
        // network has the internet connection
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Returns a Network object corresponding to
            // the currently active default data network.
            val network = connectivityManager.activeNetwork ?: return false

            // Representation of the capabilities of an active network.
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                // Indicates this network uses a Wi-Fi transport,
                // or WiFi has network connectivity
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true

                // Indicates this network uses a Cellular transport. or
                // Cellular has network connectivity
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true

                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }
}