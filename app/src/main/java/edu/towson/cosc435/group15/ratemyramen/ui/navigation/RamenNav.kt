package edu.towson.cosc435.group15.ratemyramen.ui.navigation

import android.net.Uri
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import edu.towson.cosc435.group15.ratemyramen.ui.DeleteView
import edu.towson.cosc435.group15.ratemyramen.ui.HomeScreen
import edu.towson.cosc435.group15.ratemyramen.ui.ImportMenuScreen
import edu.towson.cosc435.group15.ratemyramen.ui.importmanual.ManualImageImportScreen
import edu.towson.cosc435.group15.ratemyramen.ui.importmanual.ManualImportScreen
import edu.towson.cosc435.group15.ratemyramen.ui.importmanual.ManualImportViewModel
import edu.towson.cosc435.group15.ratemyramen.ui.importqr.QRImportScreen
import edu.towson.cosc435.group15.ratemyramen.ui.importqr.QrImportViewModel
import edu.towson.cosc435.group15.ratemyramen.ui.ramenlist.ListScreen
import edu.towson.cosc435.group15.ratemyramen.ui.ramenlist.ListViewModel


@OptIn(ExperimentalComposeUiApi::class,
    androidx.compose.foundation.ExperimentalFoundationApi::class
)
@Composable
fun RamenNav(
    takeImage:() -> Unit,
    selectImageFromGallery:() -> Unit,
    getUri:() -> Uri?,
    setUri:() -> Unit,
    navController: NavHostController = rememberNavController(),
){
    val ramenListVM: ListViewModel = viewModel()
    val ramen by ramenListVM.ramen
    val deleteVM: DeleteView = viewModel()
    val manualImportViewModel: ManualImportViewModel = viewModel()
    val qrImportViewModel: QrImportViewModel = viewModel()
    NavHost(
        navController = navController,
        startDestination = Routes.HomeScreen.route
    ){
        composable(Routes.HomeScreen.route){
            HomeScreen(navController = navController)
        }
        composable(Routes.ImportMenuScreen.route){
            ImportMenuScreen(navController = navController)
        }
        composable(Routes.ListScreen.route){

            ListScreen(navController = navController, ramen, deleteVM,
                onDelete = ramenListVM::deleteRamen, onSelect = ramenListVM::selectRamen,
                onFilter = ramenListVM::filter)
        }

        composable(Routes.ManualImportScreen.route){
            if (qrImportViewModel.tempRamenApiObject.imageUrl == "null" || getUri() != null ){
                manualImportViewModel.setImageUri(getUri())
            }
            ManualImportScreen(
                navController = navController,
                manualImportViewModel,
                setUri = setUri,
                onAddRamen = { ramen ->
                    ramenListVM.addRamen(ramen)
                    navController.navigate(Routes.ListScreen.route){
                        popUpTo((Routes.ListScreen.route))
                    }
                }
            )
        }

        composable(Routes.ManualImageImportScreen.route){
        ManualImageImportScreen(
            navController = navController,
            takeImage = takeImage,
            selectImageFromGallery = selectImageFromGallery
        )
        }

        composable(Routes.QRScanScreen.route){
            QRImportScreen(navController, manualImportViewModel, qrImportViewModel )
        }

    }
}
