package edu.towson.cosc435.group15.ratemyramen.ui

import android.app.Application
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class DeleteView(app: Application): AndroidViewModel(app), DeleteVM {
    private val _displayMessage: MutableState<Boolean> = mutableStateOf(false)
    private var _onDeleteConfirm: (suspend () -> Unit)? = null
    override val displayMessage: State<Boolean> = _displayMessage

    override fun displayDeleteConfirm(confirm : suspend () -> Unit){
        _displayMessage.value = true
        _onDeleteConfirm = confirm
    }
    override fun onDeleteConfirm(){
        if(_onDeleteConfirm != null){
            viewModelScope.launch{
                _onDeleteConfirm?.invoke()
                dismissMessage()
            }
        }
    }
    override fun dismissMessage(){
        _displayMessage.value = false;
    }
}

interface DeleteVM{
    val displayMessage: State<Boolean>
    fun displayDeleteConfirm(onConfirm: suspend () -> Unit)
    fun onDeleteConfirm()
    fun dismissMessage()
}