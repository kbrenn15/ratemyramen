package edu.towson.cosc435.group15.ratemyramen.data;

import android.app.Application;
import androidx.room.Room;
import edu.towson.cosc435.group15.ratemyramen.model.Ramen

class RamenDBRepo(app: Application): RepoInterface {
    private val ramenDb: RamenDB

    init {
        ramenDb = Room.databaseBuilder(
            app,
            RamenDB::class.java,
            "ramen_object.db"
        ) .fallbackToDestructiveMigration().build()

    }

    override suspend fun getRamen(): List<Ramen> {
        return ramenDb.ramenDao().getRamen()
    }

    override suspend fun addRamen(ramen: Ramen) {
        ramenDb.ramenDao().addRamen(ramen)
    }

    override suspend fun setRating(ramen: Ramen, rating: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteRamen(ramen: Ramen) {
        ramenDb.ramenDao().deleteRamen(ramen)
    }
}
